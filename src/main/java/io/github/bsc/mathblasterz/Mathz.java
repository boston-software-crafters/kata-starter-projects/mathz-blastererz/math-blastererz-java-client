package io.github.bsc.mathblasterz;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Mathz {
    static final String answer(String question) {
        return "2";
    }

    public static void main(String[] args) throws IOException {
        String TEAM_ID = System.getenv("MATH_TEAM_ID");

        String state = Schema.State.READY;

        while (Schema.State.running(state)) {
            String questionAsJson = JavaNet.POST.responseBody(
                    API.MATHZ,
                    API.Question.requestBody(TEAM_ID)
            );

            String answer = answer(
                    Schema.question(
                            questionAsJson
                    )
            );

            String answerAsJson = JavaNet.POST.responseBody(
                    API.MATHZ,
                    API.Answer.requestBody(TEAM_ID, answer)
            );

            state = Schema.state(answerAsJson);
        }
    }

    static class API {
        static final String MATHZ = "http://mathz-blastererz.org/api/mathz";

        static class Question {
            static String requestBody(String teamId) {
                return FakeJson.object(
                        Collections.singletonMap(
                                Schema.ACCOUNT_ID,
                                teamId
                        )
                );
            }
        }

        static class Answer {
            static String requestBody(String teamId, String answer) {
                Map<String,String> data = new HashMap<>();
                data.put(Schema.ACCOUNT_ID, teamId);
                data.put(Schema.ANSWER, answer);

                return FakeJson.object(data);
            }
        }
    }

    static class Schema {
        static final String ACCOUNT_ID = "account_uuid";
        static final String ANSWER = "answer";

        static String state(String responseBody) {
            // TODO: this should be done with a real json parser
            return match(
                    Patterns.state,
                    responseBody
            );
        }

        private static String match(Pattern p, String responseBody) {
            Matcher matcher = p.matcher(responseBody);
            if (matcher.find()) {
                return matcher.group(1);
            }
            throw new AssertionError(responseBody);
        }


        static String question(String responseBody) {
            return match(
                    Patterns.question,
                    responseBody
            );
        }

        static class Patterns {
            static final Pattern state = Pattern.compile("\"state\":\"([^\"]+)\"");
            static final Pattern question = Pattern.compile("\"question\":\"([^\"]+)\"");
        }

        @SuppressWarnings("unused")
        static class State {
            static final String READY = "Ready";
            static final String INCORRECT = "Incorrect!";
            static final String IN_PROGRESS = "In Progress";
            static final String CORRECT = "Correct!";
            static final String TIMED_OUT = "Timed Out!";
            static final String WINNER = "A Winner Is You!";

            static final List<String> RunningStates = Arrays.asList(
                    State.READY,
                    State.CORRECT,
                    State.IN_PROGRESS
            );

            static boolean running(String state) {
                return RunningStates.contains(state);
            }
        }
    }
}
