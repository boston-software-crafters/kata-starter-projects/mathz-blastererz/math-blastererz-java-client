package io.github.bsc.mathblasterz;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Danil Suits (danil@vast.com)
 */
class FakeJson {
    static String object(String key, String value) {
        return object(
                property(key, value)
        );
    }

    static String object(String properties) {
        return "{" + properties + "}";
    }

    static String object(Map<String, String> source) {
        List<String> data = source.entrySet().stream().map(
                FakeJson::property
        ).collect(Collectors.toList());

        return object(
                String.join(",", data)
        );
    }

    static String property(Map.Entry<String, String> entry) {
        return property(
                entry.getKey(),
                entry.getValue()
        );
    }

    static String property(String key, String value) {
        return "\"" + key + "\"" + ":" +
                "\"" + value + "\"";
    }
}
