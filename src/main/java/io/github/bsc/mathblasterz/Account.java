package io.github.bsc.mathblasterz;

import java.io.IOException;

class Account {
    static class Request {
        static String json(String teamName) {
            // TODO: Add validation to ensure that this
            //  client can be used by Bobby Tables.
            return  FakeJson.object(Schema.NAME, teamName);
        }

        static class Schema {
            static final String NAME = "name";
        }
    }

    public static void main(String[] args) throws IOException {

        String uri = "http://mathz-blastererz.org/api/account";
        String request = Request.json("Your Team Name Here");

        String response = JavaNet.POST.responseBody(uri, request);
        System.out.println(response);
    }
}
